---
title: Nextcloud för företaget - hur funkar det?
date: 2024-04-05
subtitle: Mötet presenterar Dwellirs erfarenheter av att välja en lösning baserat på open source-lösningen Nextcloud för företaget.
tags: [kalenderium, blogg]
---

### Dwellir och Nextcloud för företaget - hur funkar det?

Dwellir AB är ett företag inom kryptoindustrin grundat 2021 med c:a 15 personer, anställda och konsulter. Dwellir använder Nextcloud som helhetslösning för företagets kontors-IT, d.v.s. e-post, kalender, videokonferens, planering, fildelning och dokumenthantering. På det här webinariet med Open Source Sweden berättar Erik Lönroth (CTO) vad som låg till grund för beslutet att välja Nextcloud, vad de anställda tycker och hur det fungerar idag. 
Det kommer att visas exempel och det kommer finnas tid för frågor och diskusion om Dwellirs erfarenheter av att välja en lösning baserad på open source-lösningen Nextcloud för företaget.

Länk för att ansluta eventet finns på vår [LinkedIn-sida](https://www.linkedin.com/posts/open-source-sweden_inneh%C3%A5ll-dwellir-ab-%C3%A4r-ett-f%C3%B6retag-inom-activity-7170700942856634369-p3AX/)
