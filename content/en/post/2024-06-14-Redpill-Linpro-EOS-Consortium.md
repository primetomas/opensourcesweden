---
title: Redpill Linpro founds the European Open Source Consortium
date: 2024-06-14
subtitle: Open Source Sweden member Redpill Linpro has together with some European Open Source companies founded the European Open Source Consortium (EOS Consortium)
tags: [blogg]
---

### European Open Source Consortium
Redpill Linpro is a co-founder of the “European Open Source Consortium” together with our trusted partners Smile (France), Adfinis (Switzerland), Univention (Germany) and Lunatech Consulting Ltd (NL, UK and FR).

Together, the organizations have operations in over 15 European countries and more than 3,000 digitization specialists in the region, with a focus on consulting, development, optimization and operation of large-scale open source projects. Through a framework agreement with one of the consortium members, customers can gain access to the collective range of expertise provided by all consortium members.

The consortium is working together to address the need for a pan-European digitization partner and enterprise-level service provider specializing in open source and open standards, with a focus on businesses and public administrations. "With the European Open Source Consortium, we remove one of the most common objections to acquiring and introducing large-scale implementation projects based on open source - that there are no European open source partners at the enterprise level," says Fredrik Svensson, Business Development Manager at Redpill Linpro .

For more information and to see how EOS Consortium can help your organization, visit our website: https://eos-consortium.com and follow us on LinkedIn: https://lnkd.in/dXAmmJ-j
