---
title: Open Source Sweden presents at DevOps Malmö
date: 2023-11-22
subtitle: Join us at Foo Café 14th of december to talk about open source 
tags: [kalenderium, blogg]
---

### Open Source Sweden presenting at DevOps Malmö

Open Source Sweden's board members Mathias Lindroth and Magnus Glantz will present at DevOps Malmö's next meetup at Foo Café in Malmö, 14th of december, 17:30.

The speakers will walk you through all the essential things you need to know when starting or running an open source project. We will discuss fundamental considerations for successful open source projects, such as:
* Project goals and market positioning
* Project identity and brand
* Governance
* Infrastructure and financing
* Metrics and sustainability
* Licensing and all things legal

This is an useful session for anyone:
* who wants to start an open source project
* who is currently running a project and are looking for ways to improve it
* who wants to understand better how to contribute to open source projects
* who are curious of fundamentals which makes open source projects

Register here: https://www.meetup.com/devopsmalmo/events/297499298/
