# Become a support member
Email styrelsen@opensourcesweden.org with the information described in the template below to become a supporting member. Your application is then processed by the board. Anyone interested in Open Source can apply for membership.

A supporting member normally has access to the same activities and information as a full member, with the exception that a supporting member cannot vote at membership meetings. According to the statutes, the board decides which activities a support member has the right to participate in and which information he can receive.

## Current fee
(determined at the annual meeting)
Membership fee: SEK 200 / support member

## Withdrawal rules
A member who wishes to withdraw from the association must report this in writing to the association's board no later than 3 months before the turn of the year at which the membership is intended to end. Reimbursement of paid annual fee does not take place.

## Member application - support member
We need the following information to process your application:

```
Membership application - support member
The undersigned hereby applies to become a supporting member of the Association of Open Software Providers in Sweden,
Open Source Sweden.

Name
Business
Org no
Address
ZIP code
Postal address
E-mail
Phone
```
