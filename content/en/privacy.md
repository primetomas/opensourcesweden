# Privacy and GDPR
The page is designed with privacy in mind and follows best practices from PdB (Privacy by Default or Privacy by Design).

```
Our website does not collect personal data from visitors at all.
```

The page uses Hugo, which is a "static page generator". Our website therefore does not use a database but only consists of static pages.
The page does not collect and process any private data from the page's visitors and is configured not to use cookies, trackers or third-party integrations.

For questions about privacy and our website, contact styrelsen@opensourcesweden.se.

# Encryption
All traffic to the website is done over strongly encrypted HTTPS. We force all traffic to be encrypted.

Our certificate for opensourcesweden.org comes from https://letsencrypt.org/ and the renewal of the certificate is handled automatically by GitLab. We use 2048 bit Elliptic Curve for the website's certificate and the private key is protected by 4096 bit RSA.

# Don't take our word for it
You can see for yourself how we have deactivated all tracking technology on the website.

* Review how we configured the website: https://gitlab.com/opensourcesweden/home/-/blob/master/config.toml
* Review all program code for the home page: https://gitlab.com/opensourcesweden/home/
* Review which changes have been built into the homepage: https://gitlab.com/opensourcesweden/home/-/pipelines
